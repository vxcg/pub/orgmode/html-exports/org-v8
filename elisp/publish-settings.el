;;; publish-settings.el

(require 'org)
(require 'ob-tangle)
(autoload 'org-babel-tangle-file/publish-dir
  "tangle-with-publish-dir" "Tangle preserving the publishing directory")


(defun tangle-wrapper (plist filename pub-dir)
  (org-babel-tangle-file/publish-dir filename pub-dir))


;;; set coding system to UTF-8
(set-language-environment "UTF-8")

(message "Org version = %s" (org-version))
(message "Org version = %s" (org-version))
(message "Org version = %s" (org-version))
(message "load-path = %s" load-path)

(setq org-export-babel-evaluate nil)
(setq org-export-allow-BIND t)
;;; tangle code before publishing
(add-hook 'org-publish-before-export-hook 'org-babel-tangle 'append)
;(add-hook 'org-publish-before-export-hook 'org-babel-tangle)

;(setq org-babel-default-header-args
;      (cons '(:mkdirp . "yes")
;	    ;;; if mkdirp is already there, delete it
;	    (assq-delete-all :mkdirp org-babel-default-header-args)))

(setq org-babel-default-header-args
      (append '((:eval . "no") (:mkdirp . "yes"))
	    ;;; if mkdirp is already there, delete it
	    ;;; if eval is already there, delete it
	    ;;; if org-src-preserve-indentation is already there, delete it
	    (progn
	      (assq-delete-all :mkdirp  org-babel-default-header-args)
	      (assq-delete-all :eval    org-babel-default-header-args))))


;;; https://groups.google.com/forum/#!topic/comp.emacs/iiYJL04M7lA
;;; eliminate annoying messages from emacs about following
;;; version controlled files that are symlinks.
(setq vc-follow-symlinks t)

; Preserve whitespace indentation for .yml files
(setq org-src-preserve-indentation t)


;; Set to true to use regular expressions to expand noweb references.
;; This results in much faster noweb reference expansion but does
;; not properly allow code blocks to inherit the \":noweb-ref\"
;; header argument from buffer or subtree wide properties.")
(setq org-babel-use-quick-and-dirty-noweb-expansion t)

;;; https://stackoverflow.com/questions/698562/disabling-underscore-to-subscript-in-emacs-org-mode-export
;;; subscript and superscripts are suppressed
(setq org-export-with-sub-superscripts nil)

;;; default-directory :: emacs defined variable.  Is equal
;;; to the directory from where emacs is launched.
;;; base-dir  =  default-dir
;;; src-dir   =  base-dir/src
;;; build-dir =  base-dir/build
;;; docs-dir  =  build-dir/docs
;;; code-dir  =  build-dir/code

;;; root directory 
(defvar *base-dir*  default-directory)
;;; directory where sources (org) reside
(defvar *src-dir*   (concat *base-dir* "src/"))
;;; root directory of build
(defvar *build-dir* (concat *base-dir* "build/"))
;;; directory where html files reside
(defvar *docs-dir*  (concat *build-dir* "docs/"))
;;; directory where code resides
(defvar *code-dir*  (concat *build-dir* "code/"))


(message "======================")
(message "base dir = %s" *base-dir*)
(message "src dir = %s" *src-dir*)
(message "build dir = %s" *build-dir*)
(message "docs dir= %s" *docs-dir*)
(message "code dir= %s" *code-dir*)
(message "======================")

(provide 'publish-settings)

