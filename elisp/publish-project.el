;;; publish-project.el

;;; Adapted from Sebastian Rose's org-publish.el and org publishing
;;; tutorial at
;;; http://orgmode.org/worg/org-tutorials/org-publish-html-tutorial.html

;;; Customized for use by vlead-system team.

;; Maintainer: Venkatesh Choppella <venkatesh.choppell@iiit.ac.in>
;;  VLEAD  <engg@vlabs.ac.in>
;; Keywords: publish, org

;; It is released under the same terms, namely the GPL v2 or
;; later.

;; This software is distributed in the hope that it will be
;; useful, but WITHOUT ANY WARRANTY; without even the
;; implied warranty of MERCHANTABILITY or FITNESS FOR A
;; PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

(require 'publish-settings)
;;; publishing components
;;; --------------------

;;; documents
(defvar org-docs '())

;;; static stuff like images etc.
(defvar org-static '())

;;; tangled-code
(defvar org-tangled '())

;;; main project
(defvar prj '())

;;; exports  org files html and leaves them in  docs
(setq org-docs
	  `("org-docs"
		:base-directory ,*src-dir*
		:base-extension "org"
		:publishing-directory ,*docs-dir*
		:recursive t
		:publishing-function org-html-publish-to-html
		:headline-levels 4		  ; Just the default for this project.
		:auto-preamble t
		:auto-sitemap t
		:sitemap-title " "
		))
;;; copies non org (static) files  to docs
(setq org-static
	  `("org-static"
		:base-directory ,*src-dir*
		:base-extension "png\\|css\\|js\\|txt\\|jpg\\|svg\\|pdf"
		:publishing-directory ,*docs-dir*
		:recursive t
		:publishing-function org-publish-attachment
		))

;;; tangles code out from the *src-dir*
(setq org-code
	  `("org-code"
		:base-directory ,*src-dir*
		:base-extension "org"
		:publishing-directory ,*code-dir*
		:recursive t
		:publishing-function tangle-wrapper
		))

(setq prj
      '("prj" :components 
		(
		 "org-docs" 
		 "org-static" 
		 "org-code"
		 )))

(require 'ox-publish)
;;; (load-file "./elisp/htmlize.el")

(setq org-publish-project-alist
      (list org-docs org-static org-code org-tangled prj))


(defun publish-project ()
  "publish project"
  (interactive)
  (org-publish-project
   prj  ; project name
   t ; force
   ))

(provide 'publish-project)
