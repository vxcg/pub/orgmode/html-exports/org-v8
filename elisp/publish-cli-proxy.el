;;; Author: Venkatesh Choppella <venkatesh.choppell@iiit.ac.in>

;;; Command Line proxy for publishing

;;; Usage:
;;; ------
;;; 
;;;     emacs -q --script elisp/cli-proxy.el <prj-dir> <elpa-dir>
;;;
;;;      where
;;;        <prj-dir>:   project directory, defaults to
;;;                    default-directory, i.e., directory from
;;;                    where emacs is invoked.
;;;        <elpa>:     dir with user's packages, usually the elpa
;;;                    directory of the user's emacs installation.


;;; Assumptions
;;; <prj-dir>/exp holds the exporter

;;; When run this file sets up load-path and then calls
;;; publish-project function of publish-project.el

;;; The arguments are

;;;  1. prj-dir : The directory containing the project.  The
;;;                 assumption is that it contains the exporter
;;;                 directory called exp

;;;  2. <elpa-dir> :       The directory containing user packages
;;;                        (typically the elpa directory of your emacs
;;;                        installation.  We are relying currently on
;;;                        the assumption that this directory contains
;;;                        all the additional libraries needed by
;;;                        org-ref.)

;;; Here is the structure of emacs directories assumed under *prj-dir*
;;;   *prj-dir*/
;;;      exp/
;;;        elisp/
;;;        git/
;;;          org-ref/               ; cloned
;;;          emacs-htmlize/         ; cloned   
;;;        org-distro/              ; symlinked to the installed org distro 

;;; The load-path consists of the following:

;;; 1. elisp
;;; 2. All directories under git
;;; 3. lisp and contrib/lisp directories of org-distro
;;; 4. all packages under <package-user-dir>


(setq *prj-dir*
	  (if argv
		  (file-name-as-directory (elt argv 0))
		default-directory))

(require 'package)


(setq *base-dir* *prj-dir*)

(setq *exp-dir*
	  (file-name-as-directory
	   (concat
	  	*prj-dir* "exp")))

(setq exp-git-dir
	  (file-name-as-directory (concat *exp-dir* "git")))

(setq elisp-dir (concat *exp-dir* "elisp"))

(setq org-distro-dir (file-name-as-directory
					  (concat *exp-dir*
							  "org-distro")))

(setq org-distro-lisp-dirs
	  (mapcar
	   (lambda (x)
		 (concat org-distro-dir x))
	   '("lisp" "contrib/lisp")))

;; return full pathnames of directories under exp-git-dir
(setq exp-git-lisp-dirs
	  (directory-files exp-git-dir t))
	   
;;; package-user-dir is an emacs defined variable
;;; which denotes the location where user-packages are located

;;; elpa-dir is the second argument, nil otherwise
(setq elpa-dir
	  (and argv (= (length argv) 2) (elt argv 1)))

;;; set package-user-dir if elpa-dir is non-nil.
(and elpa-dir (setq package-user-dir elpa-dir))

(message "elpa-dir = %s" elpa-dir)
(message "package-user-dir = %s" package-user-dir)

;;; all directories under package-user-dir are picked up to be in the
;;; load-path



;;; put the elisp-dir git-lisp-dirs and distro ahead of everything
;;; else in the load-path.

(package-initialize)


(setq load-path
	  (append
	   (list elisp-dir)
	   exp-git-lisp-dirs
	   org-distro-lisp-dirs
	   load-path))

(print load-path)

(autoload 'publish-project
  "publish-project" "publish project")

(publish-project)




